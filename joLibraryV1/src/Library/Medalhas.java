package Library;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



/**
 * TRABALHO PRÁTICO DE PPROG
 * Licenciatura em Eng Informática (2011/2012)
 * 
 * @autores: João Carreira e João Silva
 */


/* Classe MEDALHAS: cria objecto medalhas, que irá caracterizar as melhadas
 * ganhas por cada delegação durante os Jogos Olímpicos.
 */

public class Medalhas implements Serializable {

    //  *** VARIÁVEIS DE CLASSE ***

    
    //  *** VARIÁVEIS DE INSTÂNCIA ***

    private int ouro;
    private int prata;
    private int bronze;

    //  *** CONSTRUTORES ***

    public Medalhas(int ouro, int prata, int bronze){
        setOuro(ouro);
        setPrata(prata);
        setBronze(bronze);
    }
    
    public Medalhas(){
        
    }

    //  *** MÉTODOS DE CLASSE ***
    
    /* Conta total de medalhas obtidas por um país
     */
    public static int totalMedalhas(Delegacao d){
        return d.getMedalhas().getOuro()+d.getMedalhas().getPrata()+d.getMedalhas().getBronze();
    }
    
    /* Conta total de medalhas obtidas por um país mas tem em conta o peso
     * atribuído a cada medalha (3 para ouro, 2 para prata, 1 para bronze)
     */
    public static int totalMedalhasQual(Delegacao d){
        int x1=3, x2=2;
        return x1*d.getMedalhas().getOuro()+x2*d.getMedalhas().getPrata()+d.getMedalhas().getBronze();
    }
    public static int[] arrayMedalhas(Delegacao d){
        int[] a = new int [3];
        a[0]=d.getMedalhas().getOuro();
        a[1]=d.getMedalhas().getPrata();
        a[2]=d.getMedalhas().getBronze();
        return a;
    }
    
      public static int totalOuro(Delegacao d){
        return d.getMedalhas().getOuro();
    }
      public static int totalPrata(Delegacao d){
        return d.getMedalhas().getPrata();
    }
      public static int totalBronze(Delegacao d){
        return d.getMedalhas().getBronze();
    }
     /* Faz a classifição dos países, por ordem descrescente; recebe como parâmetro
     * array contendo objectos Delegações e ordena por total de medalhas; retornna
     * array ordenado
     */
    public static ArrayList<Delegacao> classifPorDeleg(ArrayList<Delegacao> a){
        Comparator c = new Comparator(){
            @Override
            public int compare(Object o1, Object o2){
                int i1 = (totalMedalhas(((Delegacao)o1)));
                int i2 = (totalMedalhas(((Delegacao)o2)));
                if(i1>i2)
                    return -1;
                else if(i1<i2)
                    return 1;
                else
                    return 0;
            }
        };
        Collections.sort(a,c);
        return a;
    }
    public static ArrayList<Delegacao> classifPorMedalha(ArrayList<Delegacao> a){
        Comparator c = new Comparator(){
            @Override
            public int compare(Object o1, Object o2){
                int i1 = (totalOuro(((Delegacao)o1)));
                int i2 = (totalOuro(((Delegacao)o2)));
                if(i1>i2)
                    return -1;
                else if(i1<i2)
                    return 1;
                else if (i1==i2){
                        int f1 = (totalPrata(((Delegacao)o1)));
                        int f2 = (totalPrata(((Delegacao)o2)));
                            if(i1>i2)
                                return -1;
                            else if(i1<i2)
                                return 1;
                            else if (i1==i2){
                                int z1 = (totalBronze(((Delegacao)o1)));
                                int z2 = (totalBronze(((Delegacao)o2)));
                                     if(i1>i2)
                                                    return -1;
                                     else if(i1<i2)
                                                    return 1;
                                     else 
                                                    return 0;
                            }
                }
                    return 0;
            }
        };
        Collections.sort(a,c);
        return a;
    }
   
    /* Listar paises sem medalhas
     * 
     */
//    public static ArrayList<Delegacao> delegSemMedalhas(ArrayList<Delegacao> a){
//        ArrayList<Delegacao> semMedalhas = a;
//        for(Delegacao d : semMedalhas){
//            if(totalMedalhas(d)==0)
//                a.remove(d);
//        }
//        return semMedalhas;
//    }
    
    /* Faz a classifição dos países por TOTAL QUALIFICADO, por ordem descrescente; recebe como parâmetro
     * array contendo objectos Delegações e ordena por total de medalhas; retornna
     * array ordenado
     */
     public static ArrayList<Delegacao> classifPorDelegQual(ArrayList<Delegacao> a){
        Comparator c = new Comparator(){
            @Override
            public int compare(Object o1, Object o2){
                int i1 = (totalMedalhasQual(((Delegacao)o1)));
                int i2 = (totalMedalhasQual(((Delegacao)o2)));
                if(i1>i2)
                    return -1;
                else if(i1<i2)
                    return 1;
                else
                    return 0;
            }
        };
        Collections.sort(a,c);
        return a;
    }
    
    
    //  *** MÉTODOS DE INSTÂNCIA ***

    //      *** MÉTODOS DE CONSULTA (GETS) ***

    public int getOuro(){
        return ouro;
    }
    
    public int getPrata(){
        return prata;
    }
    
    public int getBronze(){
        return bronze;
    }
        
    //      *** MÉTODOS DE MODIFICAÇÃO (SETS) ***

    public void setOuro(int ouro){
        this.ouro = ouro;
    }
    
    public void setPrata(int prata){
        this.prata = prata;
    }
    
    public void setBronze(int bronze){
        this.bronze = bronze;
    }
        
    //      *** MÉTODOS COMPLEMENTARES E AUXILIARES ***

    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("\nOuro: ");
        s.append(ouro);
        s.append("\nPrata: ");
        s.append(prata);
        s.append("\nBronze: ");
        s.append(bronze);
        return s.toString();
    }
}
