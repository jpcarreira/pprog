package Library;

import java.io.Serializable;

public class Modalidade implements Serializable {

    //  *** VARIÁVEIS DE CLASSE ***

    
    //  *** VARIÁVEIS DE INSTÂNCIA ***

    private String modalidade;
    private String prova;

    //  *** CONSTRUTORES ***

    // construtor para modalidade com várias provas
    public Modalidade(String modalidade, String prova){
        setModalidade(modalidade);
        setProva(prova);
    }
    
    // construtor para modalidade sem provas
    public Modalidade(String modalidade){
        setModalidade(modalidade);
    }

    //  *** MÉTODOS DE CLASSE ***


    //  *** MÉTODOS DE INSTÂNCIA ***

    //      *** MÉTODOS DE CONSULTA (GETS) ***

    public String getModalidade() {
        return modalidade;
    }    
    
    public String getProva() {
        return prova;
    }
    
    //      *** MÉTODOS DE MODIFICAÇÃO (SETS) ***

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

    public void setProva(String prova) {
        this.prova = prova;
    }
    
        
    //      *** MÉTODOS COMPLEMENTARES E AUXILIARES ***

    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("\nModalidade: ");
        s.append(modalidade);
        if(getProva()!=null){
            s.append("\nProva: ");
            s.append(prova);
        }    
        return s.toString();
    }
    
    

}
