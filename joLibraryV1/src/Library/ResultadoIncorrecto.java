package Library;


public class ResultadoIncorrecto extends Exception {
    
    public ResultadoIncorrecto(){
        super("Resultado incorrecto!");
    }
    
    public ResultadoIncorrecto(String s){
        super("Resultado incorrecto! \nNão foi atribuído resultado à "+s);
    }


}
