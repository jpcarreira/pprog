package Library;


import java.io.Serializable;
import java.util.ArrayList;


public class Resultado implements Serializable {

    //  *** VARIÁVEIS DE CLASSE ***

    
    //  *** VARIÁVEIS DE INSTÂNCIA ***

    private Delegacao delegacao;
    private Modalidade modalidade;
    private int result;

    //  *** CONSTRUTORES ***

    public Resultado(Delegacao delegacao, Modalidade modalidade, int result) throws ResultadoIncorrecto {
        setDelegacao(delegacao);
        setModalidade(modalidade);
        setResult(result);
    }

    //  *** MÉTODOS DE CLASSE ***

    /* Acrescente medalha de ouro, prata ou bronze em funcao do resultado ser
     * 1, 2 ou 3, respectivamente
     */
    public static void atribuirMedalha(ArrayList<Resultado> r){
        for(Resultado a : r){
            if(a.getResult()==1)
                a.getDelegacao().getMedalhas().setOuro(a.getDelegacao().getMedalhas().getOuro()+1);
            else if(a.getResult()==2)
                a.getDelegacao().getMedalhas().setPrata(a.getDelegacao().getMedalhas().getPrata()+1);
            else if(a.getResult()==3)
                a.getDelegacao().getMedalhas().setBronze(a.getDelegacao().getMedalhas().getBronze()+1);   
        }
    }
    
    //  *** MÉTODOS DE INSTÂNCIA ***

    //      *** MÉTODOS DE CONSULTA (GETS) ***

    public Delegacao getDelegacao() {
        return delegacao;
    }
    
    public Modalidade getModalidade() {
        return modalidade;
    }
    
    public int getResult() {
        return result;
    }
    
    //      *** MÉTODOS DE MODIFICAÇÃO (SETS) ***

    public void setDelegacao(Delegacao delegacao) {
        this.delegacao = delegacao;
    }
    
    public void setModalidade(Modalidade modalidade) {
        this.modalidade = modalidade;
    }
    
    public void setResult(int result) throws ResultadoIncorrecto {
        if(result>0)
            this.result = result;
        else
            throw new ResultadoIncorrecto("delegação "+delegacao.getPais()+" na modalidade "+modalidade.getModalidade());
    }
    
    //      *** MÉTODOS COMPLEMENTARES E AUXILIARES ***

    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("\nDelegação: ");
        s.append(delegacao.getPais());
        s.append(modalidade);
        s.append("\nResultado: ");
        s.append(result+"º lugar");
        return s.toString();
    }

}
