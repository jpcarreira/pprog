package Library;

import java.io.Serializable;
import java.util.ArrayList;



/**
 * TRABALHO PRÁTICO DE PPROG
 * Licenciatura em Eng Informática (2011/2012)
 * 
 * @autores: João Carreira e João Silva
 */


/* Classe DELEGAÇÃO: cria objectos DELEGAÇÃO, cada um a representar um país que
 * esteja presente nos Jogos Olímpicos (caraterizado por variável país (Sring) e 
 * composta pela classe MEDALHAS, que contem todas as medalhas ganhas pela 
 * delegação.
 */

public class Delegacao implements Serializable {

    //  *** VARIÁVEIS DE CLASSE ***

    
    //  *** VARIÁVEIS DE INSTÂNCIA ***

    private String pais;
    private Medalhas medalhas;
    
    //  *** CONSTRUTORES ***

    public Delegacao(String pais, Medalhas medalhas){
        setPais(pais);
        setMedalhas(medalhas);
    }

    //  *** MÉTODOS DE CLASSE ***
    
    /* Cria uma nova delegação recebendo nome da mesma como parâmetro
     */
    public static Delegacao criarDelegacao(String s){
        return new Delegacao(s,new Medalhas());
    }
    
    /* Coloca todas as medalhas zero
     * 
     */
    public static void limparMedalhas(ArrayList<Delegacao> d){
        for(Delegacao a : d){
            a.getMedalhas().setOuro(0);
            a.getMedalhas().setPrata(0);
            a.getMedalhas().setBronze(0);
        }
    }
    
    //  *** MÉTODOS DE INSTÂNCIA ***

    //      *** MÉTODOS DE CONSULTA (GETS) ***

    public String getPais(){
        return pais;
    }
        
    public Medalhas getMedalhas(){
        return medalhas;
    }
    //      *** MÉTODOS DE MODIFICAÇÃO (SETS) ***

    public void setPais(String pais){
        this.pais = pais.toUpperCase();
    }
    
    public void setMedalhas(Medalhas medalhas){
        this.medalhas = medalhas;
    }
        
    //      *** MÉTODOS COMPLEMENTARES E AUXILIARES ***

    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("\nDelegação: ");
        s.append(pais);
        s.append("\nTotal: ");
        s.append(Medalhas.totalMedalhas(this));
        s.append(medalhas);
        return s.toString();
    }
}
