
import Library.*;
import javax.swing.*;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EmptyBorder;

public class DialogModalidade extends JDialog {

    private Modalidade modalidade;
    private JTextField txtNomeModalidade, txtNomeProva;
    private JButton btOK, btCancel;

    // construtor da janela DELEGAÇÃO
    public DialogModalidade (JFrame pai, String titulo){
        super(pai,titulo,true);
        
        // CENTER DO BORDERLAYOUT***********************************************
        // criar gridlayout com 2 linhas num JPanel
        JPanel pGrid = new JPanel(new GridLayout(2,1));
        // JPanel para o nome da modalidade
        JPanel pLinha1 = new JPanel();
        pLinha1.setBorder(new EmptyBorder(5,0,5,0));
        // JLabel para o nome
        JLabel lb1 = new JLabel("Nome da Modalidade: ");
        txtNomeModalidade = new JTextField(20);
        // adicionar label ao panel
        pLinha1.add(lb1);
        pLinha1.add(txtNomeModalidade);
        pGrid.add(pLinha1);
        
        // JPanel para o nome da prova
        JPanel pLinha2 = new JPanel();
        pLinha2.setBorder(new EmptyBorder(5,0,5,0));
        // JLabel para o nome
        JLabel lb2 = new JLabel("Nome da Prova: ");
        txtNomeProva = new JTextField(20);
        // adicionar label ao panel
        pLinha2.add(lb2);
        pLinha2.add(txtNomeProva);
        pGrid.add(pLinha2);
        
        
        
        // *********************************************************************
        
        TrataEvento t = new TrataEvento();
        
        // SOUTH DO BORDERLAYOUT************************************************
        // criar painel para botões
        JPanel pBtn = new JPanel();
        pBtn.setBorder(new EmptyBorder(5,0,5,0));
        // botão OK
        btOK = new JButton("OK");
        btOK.addActionListener(t);
        // fazer actionlistener para OK
        pBtn.add(btOK);
        // botao CANCELAR
        btCancel = new JButton("Cancelar");
        btCancel.addActionListener(t);
        pBtn.add(btCancel);
        // colocar botao OK como default
        getRootPane().setDefaultButton(btOK);
        
        // adicionar pGrid ao CENTER do BorderLayout
        add(pGrid, BorderLayout.CENTER);
        // adicionar pBtn ao SOUTH do BorderLayout
        add(pBtn, BorderLayout.SOUTH);
        
        
        // settings para dimensoes da janela e localização
        pack();
        setMinimumSize(new Dimension(getWidth(),getHeight()));
        setLocation(pai.getX()+50, pai.getY()+50);  
    }
    
    // alteração de modalidades
    public void setModalidade(Modalidade m){
        txtNomeModalidade.setText(m.getModalidade());
        txtNomeProva.setText(m.getProva());
    }
    
    
    // tratamento de eventos
    private class TrataEvento implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // botão CANCEL
            if(e.getSource()==btCancel){
                modalidade=null;
                dispose();
            }
            // botão OK
            else{
                if(txtNomeModalidade.getText().isEmpty()){
                    JOptionPane.showMessageDialog(DialogModalidade.this,"Introduza um nome para a Modalidade","Criar Modalidade",JOptionPane.WARNING_MESSAGE);
                    txtNomeModalidade.requestFocus();
                }
                else if(txtNomeProva.getText().isEmpty()){
                    JOptionPane.showMessageDialog(DialogModalidade.this,"Introduza um nome para a Prova","Criar Modalidade",JOptionPane.WARNING_MESSAGE);
                    txtNomeProva.requestFocus();
                }
                else{
                    String nomeM = txtNomeModalidade.getText();
                    String nomeP = txtNomeProva.getText();
                    modalidade = new Modalidade(nomeM,nomeP);
                    dispose();
                }      
            }
        }
    }
    
    public Modalidade getModalidade(){
        return modalidade;
    }
    
}