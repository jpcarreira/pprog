

import Library.Medalhas;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class DialogClassificacao extends JDialog {
    
    public DialogClassificacao(JFrame pai, String titulo){
        
        // CENTER DO BORDERLAYOUT***********************************************
        JPanel pList = new JPanel(new BorderLayout());
        pList.setBorder(new EmptyBorder(10,10,10,10));
        JList list = new JList(Main.classifPorString());
        JScrollPane scroll = new JScrollPane(list);
        pList.add(scroll);
        //**********************************************************************
        
        // SOUTH DO BORDERLAYOUT************************************************
        JPanel pOK = new JPanel();
        JButton btOK = new JButton("OK");
        btOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                dispose();
            }
        });
        getRootPane().setDefaultButton(btOK);
        //**********************************************************************
        
        add(pList, BorderLayout.CENTER);
        add(pOK, BorderLayout.SOUTH);
        
        // settings gerais da janela
//        pack();
        setSize(500,300);
        setLocation(pai.getX()+50, pai.getY()+50);
        setVisible(true);
        
    }
}