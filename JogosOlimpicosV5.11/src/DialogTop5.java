
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.*;




public class DialogTop5 extends JDialog{
   
    
   
    public DialogTop5(JFrame pai,String titulo){
       super(pai, titulo, true);
       
       String[]a = Main.classifPorString2();
       String[][] temp=new String[a.length][4];
       for (int i = 0; i < a.length; i++)
          for (int j=0;j<5;j++)  
           temp[i] = a[i].split(",");
       
       JPanel border= new JPanel(new GridLayout(5,1));
       // #1
       JPanel pGrid1 = new JPanel(new GridLayout(5,1));
       JPanel n1 = new JPanel();
       JPanel linha11 = new JPanel();
       JPanel linha21 = new JPanel();
       JPanel linha31 = new JPanel();
       String nome1 = temp[0][0];
       int o1 = Integer.parseInt(temp[0][1]);
       int p1 = Integer.parseInt(temp[0][2]);
       int b1 = Integer.parseInt(temp[0][3]);
       JLabel lbl1 = new JLabel(nome1);
       n1.add(lbl1);
       JLabel gold1 = new JLabel("  Ouro:");
       JTextField txtg1 = new JTextField(o1*2);
       JLabel silver1 = new JLabel(" Prata:");
       JTextField txts1 = new JTextField(p1*2);
       JLabel bronse1 = new JLabel("Bronze:");
       JTextField txtb1 = new JTextField(b1*2);
       linha11.add(gold1);linha11.add(txtg1);
       linha21.add(silver1);linha21.add(txts1);
       linha31.add(bronse1);linha31.add(txtb1);
       pGrid1.add(n1);
       pGrid1.add(linha11);
       pGrid1.add(linha21);
       pGrid1.add(linha31);
       border.add(pGrid1);
       
       //  #2
       JPanel pGrid2 = new JPanel(new GridLayout(5,1));
       JPanel n2 = new JPanel();
       JPanel linha12 = new JPanel();
       JPanel linha22 = new JPanel();
       JPanel linha32 = new JPanel();
       String nome2 = temp[1][0];
       int o2 = Integer.parseInt(temp[1][1]);
       int p2 = Integer.parseInt(temp[1][2]);
       int b2 = Integer.parseInt(temp[1][3]);
       JLabel lbl2 = new JLabel(nome2);
       n2.add(lbl2);
       JLabel gold2 = new JLabel("  Ouro:");
       JTextField txtg2 = new JTextField(o2*2);
       JLabel silver2 = new JLabel(" Prata:");
       JTextField txts2 = new JTextField(p2*2);
       JLabel bronse2= new JLabel("Bronze:");
       JTextField txtb2 = new JTextField(b2*2);
       linha12.add(gold2);linha12.add(txtg2);
       linha22.add(silver2);linha22.add(txts2);
       linha32.add(bronse2);linha32.add(txtb2);
       pGrid2.add(n2);
       pGrid2.add(linha12);
       pGrid2.add(linha22);
       pGrid2.add(linha32);
       border.add(pGrid2);
       
       //#3
       JPanel pGrid3 = new JPanel(new GridLayout(5,1));
       JPanel n3 = new JPanel();
       JPanel linha13 = new JPanel();
       JPanel linha23 = new JPanel();
       JPanel linha33 = new JPanel();
       String nome3 = temp[2][0];
       int o3 = Integer.parseInt(temp[2][1]);
       int p3 = Integer.parseInt(temp[2][2]);
       int b3 = Integer.parseInt(temp[2][3]);
       JLabel lbl3 = new JLabel(nome3);
       n3.add(lbl3);
       JLabel gold3 = new JLabel("  Ouro:");
       JTextField txtg3 = new JTextField(o3*2);
       JLabel silver3 = new JLabel(" Prata:");
       JTextField txts3 = new JTextField(p3*2);
       JLabel bronse3 = new JLabel("Bronze:");
       JTextField txtb3 = new JTextField(b3*2);
       linha13.add(gold3);linha13.add(txtg3);
       linha23.add(silver3);linha23.add(txts3);
       linha33.add(bronse3);linha33.add(txtb3);
       pGrid3.add(n3);
       pGrid3.add(linha13);
       pGrid3.add(linha23);
       pGrid3.add(linha33);
       border.add(pGrid3);
       
       //#4
       JPanel pGrid4 = new JPanel(new GridLayout(5,1));
       JPanel n4 = new JPanel();
       JPanel linha14 = new JPanel();
       JPanel linha24 = new JPanel();
       JPanel linha34 = new JPanel();
       String nome4 = temp[3][0];
       int o4 = Integer.parseInt(temp[3][1]);
       int p4 = Integer.parseInt(temp[3][2]);
       int b4 = Integer.parseInt(temp[3][3]);
       JLabel lbl4 = new JLabel(nome4);
       n4.add(lbl4);
       JLabel gold4 = new JLabel("  Ouro:");
       JTextField txtg4 = new JTextField(o4*2);
       JLabel silver4 = new JLabel(" Prata:");
       JTextField txts4 = new JTextField(p4*2);
       JLabel bronse4 = new JLabel("Bronze:");
       JTextField txtb4 = new JTextField(b4*2);
       linha14.add(gold4);linha14.add(txtg4);
       linha24.add(silver4);linha24.add(txts4);
       linha34.add(bronse4);linha34.add(txtb4);
       pGrid4.add(n4);
       pGrid4.add(linha14);
       pGrid4.add(linha24);
       pGrid4.add(linha34);
       border.add(pGrid4);
       
       //#5
       JPanel pGrid5 = new JPanel(new GridLayout(5,1));
       JPanel n5 = new JPanel();
       JPanel linha15 = new JPanel();
       JPanel linha25 = new JPanel();
       JPanel linha35 = new JPanel();
       String nome5 = temp[4][0];
       int o5 = Integer.parseInt(temp[4][1]);
       int p5 = Integer.parseInt(temp[4][2]);
       int b5 = Integer.parseInt(temp[4][3]);
       JLabel lbl5 = new JLabel(nome5);
       n5.add(lbl5);
       JLabel gold5 = new JLabel("  Ouro:");
       JTextField txtg5 = new JTextField(o5*2);
       JLabel silver5 = new JLabel(" Prata:");
       JTextField txts5 = new JTextField(p5*2);
       JLabel bronse5 = new JLabel("Bronze:");
       JTextField txtb5 = new JTextField(b5*2);
       linha15.add(gold5);linha15.add(txtg5);
       linha25.add(silver5);linha25.add(txts5);
       linha35.add(bronse5);linha35.add(txtb5);
       pGrid5.add(n5);
       pGrid5.add(linha15);
       pGrid5.add(linha25);
       pGrid5.add(linha35);
       border.add(pGrid5);
       
       
       add(border, BorderLayout.CENTER);
                    
       pack();
       setMinimumSize(new Dimension(getWidth(), getHeight()));
       setLocation(pai.getX() + 100, pai.getY() + 100);
       setVisible(true);
       
}}
