
import Library.*;
import javax.swing.*;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EmptyBorder;

public class DialogMedalhasDelegacao extends JDialog {

    private JComboBox comboDeleg;
    private JLabel lbDelegacao, lbOuro, lbPrata, lbBronze;

    // construtor da janela DELEGAÇÃO
    public DialogMedalhasDelegacao (JFrame pai, String titulo){
        super(pai,titulo,true);
  
        JPanel pCombo = new JPanel(new GridLayout(2,1));
        pCombo.setBorder(new EmptyBorder(5,0,5,0));
        JLabel lb1 = new JLabel("Escolha a delegação: ");
        lb1.setHorizontalAlignment(SwingConstants.CENTER);
        pCombo.add(lb1);
        
        TrataEvento t = new TrataEvento();
        
        comboDeleg = new JComboBox(Main.nomesDelegacoes());
        comboDeleg.setSelectedIndex(-1);
        comboDeleg.setMaximumRowCount(5);
        comboDeleg.addActionListener(t);
        pCombo.add(comboDeleg);
        
        JPanel pTexto = new JPanel(new GridLayout(4,1));
        lbDelegacao = new JLabel("");
        lbDelegacao.setHorizontalAlignment(SwingConstants.CENTER);
        lbOuro = new JLabel("");
        lbOuro.setHorizontalAlignment(SwingConstants.CENTER);
        lbPrata = new JLabel("");
        lbPrata.setHorizontalAlignment(SwingConstants.CENTER);
        lbBronze = new JLabel("");
        lbBronze.setHorizontalAlignment(SwingConstants.CENTER);
        pTexto.add(lbDelegacao);
        pTexto.add(lbOuro);
        pTexto.add(lbPrata);
        pTexto.add(lbBronze);
        
        JPanel pOk = new JPanel();
        pOk.setBorder(new EmptyBorder(10,0,10,0));
        JButton btOk = new JButton("OK");
        btOk.addActionListener(t);
        pOk.add(btOk);
        
        getRootPane().setDefaultButton(btOk);
        
        add(pCombo, BorderLayout.NORTH);
        add(pTexto, BorderLayout.CENTER);
        add(pOk, BorderLayout.SOUTH);
        
        
        setMinimumSize(new Dimension(300,300));
        setLocation(pai.getX()+50,pai.getY()+50);
        setVisible(true);
    }
        
   
    // tratamento de eventos
    private class TrataEvento implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource()==comboDeleg){
                Delegacao del = Main.getDelegacao().get(comboDeleg.getSelectedIndex());
                lbDelegacao.setText("Delegação: "+del.getPais());
                lbOuro.setText("Medalhas de Ouro: "+del.getMedalhas().getOuro());
                lbPrata.setText("Medalhas de Prata: "+del.getMedalhas().getPrata());
                lbBronze.setText("Medalhas de Bronze: "+del.getMedalhas().getBronze());
            }
            else
                dispose();
        }
    }
    }
    
