
import Library.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Main {

    private static Janela janela = new Janela("Gestão de Medalhas Olímpicas v5.9");
    private static ArrayList<Delegacao> delegacao = new ArrayList<Delegacao>();
    private static ArrayList<Resultado> resultado = new ArrayList<Resultado>();
    private static ArrayList<Modalidade> modalidade = new ArrayList<Modalidade>();
    
    
    
    public static void main(String[] args){
        lerEstadoAnterior();
    }

    
    // retornar arraylist<Delegacao> para janelas da GUI
    public static ArrayList<Delegacao> getDelegacao(){
        return delegacao;
    }
    
    // retornar arraylist<Resultado> para janelas da GUI
    public static ArrayList<Resultado> getResultado(){
        return resultado;
    }
    
    // retornar arraylist<Modalidade> para janelas da GUI
    public static ArrayList<Modalidade> getModalidade(){
        return modalidade;
    }
    
    // método que retorna array de strings com nomes de todas as delegações (usado para listar delegações)
    public static String[] nomesDelegacoes(){
        String[] a = new String[delegacao.size()];
        for(int i=0; i<a.length; i++){
            String nome = delegacao.get(i).getPais();
            a[i]=String.format(nome);
        }
        return a;
    }
    
     // método que retorna array de strings com nomes de todas as modalidades (usado para listar modalidades)
    public static String[] nomesModalidades(){
        String[] a = new String[modalidade.size()];
        for(int i=0; i<a.length; i++){
            String nomeM = modalidade.get(i).getModalidade();
            String nomeP = modalidade.get(i).getProva();
            a[i]=String.format(" %s : %s ",nomeM,nomeP);
        }
        return a;
    }
    
     // método que retorna array de strings com nomes de todas as modalidades (usado para listar resultados)
    public static String[] nomesResultados(){
        String[] a = new String[resultado.size()];
        for(int i=0; i<a.length; i++){
            String nomeD = resultado.get(i).getDelegacao().getPais();
            String nomeM = resultado.get(i).getModalidade().getModalidade();
            String nomeP = resultado.get(i).getModalidade().getProva();
            int x = resultado.get(i).getResult();
            a[i]=String.format(" %s : %s - %s : %dº lugar",nomeD,nomeM,nomeP,x);
        }
        return a;
    }
    
    
    public static Delegacao delegPorNome(String s){
        String[] nomes = nomesDelegacoes();
        int i=0;
        while(!nomes[i].equalsIgnoreCase(s))
            i++;
        return delegacao.get(i);
    }
    
    public static Modalidade modalidPorNome(String s){
        String[] nomes = nomesModalidades();
        int i=0;
        while(!nomes[i].equalsIgnoreCase(s))
            i++;
        return modalidade.get(i);
    }
    
    public static Resultado resultPorNome(String s){
        String[] nomes = nomesResultados();
        int i=0;
        while(!nomes[i].equalsIgnoreCase(s))
            i++;
        return resultado.get(i);
    }
    
    public static String[] classifPorString(){
        String[] a = new String[delegacao.size()];
        for(int i=0; i<a.length; i++){
            String nome = delegacao.get(i).getPais();
            int medalha = Medalhas.totalMedalhas(delegacao.get(i));
            a[i]=String.format("%s >>>> %d medalhas",nome,medalha);
        }
        return a;
    }
    public static String[] classifPorString2(){
         String[] a = new String[delegacao.size()];
         for(int i=0; i<a.length; i++){
            String nome = delegacao.get(i).getPais();
            int[] medalha = Medalhas.arrayMedalhas(delegacao.get(i));
            a[i]=String.format("%s,%d,%d,%d",nome,medalha[0],medalha[1],medalha[2]);
        }
        return a;
    }
     public static String[] classifQualifPorString(){
        String[] a = new String[delegacao.size()];
        for(int i=0; i<a.length; i++){
            String nome = delegacao.get(i).getPais();
            int peso = Medalhas.totalMedalhasQual(delegacao.get(i));
            a[i]=String.format("%s >>>> %d pts",nome,peso);
        }
        return a;
    }
    
    // método que retorna array de strings com nomes de todas as delegações sem medalhas (usado para listar delegações)
    public static String[] nomesDelegacoesSemMedalhas(){
        String[] a = new String[delegacao.size()];
        for(int i=0; i<a.length; i++){
            if(Medalhas.totalMedalhas(delegacao.get(i))==0){
                String nome = delegacao.get(i).getPais();
                a[i]=String.format(nome);
            }
        }
        return a;
    }
     
    
    // carregar dados a partir dos ficheiros bin
    private static void lerEstadoAnterior() {
        try{
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("delegacao.bin"));
            delegacao = (ArrayList<Delegacao>) in.readObject();
            in.close();
            ObjectInputStream in2 = new ObjectInputStream(new FileInputStream("resultado.bin"));
            resultado = (ArrayList<Resultado>) in2.readObject();
            in2.close();
            ObjectInputStream in3 = new ObjectInputStream(new FileInputStream("modalidade.bin"));
            modalidade = (ArrayList<Modalidade>) in3.readObject();
            in3.close();
        }
        catch(IOException exc) {
            JOptionPane.showMessageDialog(janela,"Estado anterior não foi resposto!","Carregamento do Estado Anterior",JOptionPane.ERROR_MESSAGE);
        }
        catch(ClassNotFoundException exc) {
            JOptionPane.showMessageDialog(janela,"Estado anterior não foi resposto!","Carregamento do Estado Anterior",JOptionPane.ERROR_MESSAGE);   
        }
    }
    
    // gravar dados nos ficheiros bin
    public static void gravarEstado() {
        try{
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("delegacao.bin"));
            out.writeObject(delegacao);
            out.close();
            ObjectOutputStream out2 = new ObjectOutputStream(new FileOutputStream("resultado.bin"));
            out2.writeObject(resultado);
            out2.close();
            ObjectOutputStream out3 = new ObjectOutputStream(new FileOutputStream("modalidade.bin"));
            out3.writeObject(modalidade);
            out3.close();
        }
        catch (IOException exc){
            JOptionPane.showMessageDialog(janela,"Dados não foram gravados!","Fechar Aplicação",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    // listar delegações para ficheiro
    public static void listarDelegFicheiro(){
        try{
            Formatter out = new Formatter(new File("listaDelegacoes.txt"));
            out.format("++++++++ LISTA DE DELEGAÇÕES ++++++++");
            out.format("%n");
            for(Delegacao d : delegacao)
                out.format("%s %n",d);
            out.close();
            JOptionPane.showMessageDialog(janela,"Ficheiro criado com sucesso!","Listar Delegações em Ficheiro",JOptionPane.INFORMATION_MESSAGE);
        }
        catch(FileNotFoundException exc){
            JOptionPane.showMessageDialog(janela,"Falhou a criação do ficheiro!","Listar Delegações em Ficheiro",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    // listar delegações para ficheiro
    public static void listarModalidFicheiro(){
        try{
            Formatter out = new Formatter(new File("listaModalidades.txt"));
            out.format("++++++++ LISTA DE MODALIDADES ++++++++");
            out.format("%n");
            for(Modalidade m : modalidade)
                out.format("%s %n",m);
            out.close();
            JOptionPane.showMessageDialog(janela,"Ficheiro criado com sucesso!","Listar Delegações em Ficheiro",JOptionPane.INFORMATION_MESSAGE);
        }
        catch(FileNotFoundException exc){
            JOptionPane.showMessageDialog(janela,"Falhou a criação do ficheiro!","Listar Delegações em Ficheiro",JOptionPane.ERROR_MESSAGE);
        }
    }

    // listar resultados para ficheiro
    public static void listarResultFicheiro(){
        try{
            Formatter out = new Formatter(new File("listaResultados.txt"));
            out.format("++++++++ LISTA DE RESULTADOS ++++++++");
            out.format("%n");
            for(Resultado r : resultado)
                out.format("%s %n",r.toString());
            out.close();
            JOptionPane.showMessageDialog(janela,"Ficheiro criado com sucesso!","Listar Delegações em Ficheiro",JOptionPane.INFORMATION_MESSAGE);
        }
        catch(FileNotFoundException exc){
            JOptionPane.showMessageDialog(janela,"Falhou a criação do ficheiro!","Listar Delegações em Ficheiro",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    // carregar delegações a partir de txt
    public static void carregarDelegacoesTxt(){
        try{
            Scanner in = new Scanner(new File("del.txt"));
            if(!in.hasNextLine()){
                JOptionPane.showMessageDialog(janela, "O ficheiro está vazio!","Carregar Delegações",JOptionPane.WARNING_MESSAGE);
            }
            else{
                ArrayList<Delegacao> tmp = new ArrayList<Delegacao>();
                while(in.hasNextLine()){
                    String aux = in.nextLine();
                    tmp.add(new Delegacao(aux,new Medalhas()));        
                }
                delegacao.addAll(tmp);
                JOptionPane.showMessageDialog(janela, "Delegações Carregadas com sucesso!","Carregar Delegações",JOptionPane.INFORMATION_MESSAGE);
            } 
        }
        catch(FileNotFoundException ext){
            JOptionPane.showMessageDialog(janela, "Ficheiro não encontrado!","Carregar Delegações",JOptionPane.WARNING_MESSAGE);
        }
    }
    
    // carregar modalidades a partir de txt
    public static void carregarModaliadadesTxt(){
        try{
            Scanner in = new Scanner(new File("mod.txt"));
            if(!in.hasNextLine()){
                JOptionPane.showMessageDialog(janela, "O ficheiro está vazio!","Carregar Modalidades",JOptionPane.WARNING_MESSAGE);
            }
            else{
                ArrayList<Modalidade> tmp = new ArrayList<Modalidade>();
                while(in.hasNextLine()){
                    String aux[] = in.nextLine().split(";");
                    tmp.add(new Modalidade(aux[0],aux[1]));
                }
                modalidade.addAll(tmp);
                JOptionPane.showMessageDialog(janela, "Modalidades Carregadas com sucesso!","Carregar Modalidades",JOptionPane.INFORMATION_MESSAGE);
            } 
        }
        catch(FileNotFoundException ext){
            JOptionPane.showMessageDialog(janela, "Ficheiro não encontrado!","Carregar Modalidades",JOptionPane.WARNING_MESSAGE);
        }
    }
    
    
 
    
    
}
