
import Library.*;
import javax.swing.*;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EmptyBorder;

public class DialogDelegacao extends JDialog {

    private Delegacao delegacao;
    private JTextField txtNomeDelegacao;
    private JButton btOK, btCancel;

    // construtor da janela DELEGAÇÃO
    public DialogDelegacao (JFrame pai, String titulo){
        super(pai,titulo,true);
        
        // CENTER DO BORDERLAYOUT***********************************************
        // criar gridlayout com 1 linha só (para o nome) num JPanel
        JPanel pGrid = new JPanel(new GridLayout(1,1));
        // JPanel para o nome
        JPanel pLinha = new JPanel();
        pLinha.setBorder(new EmptyBorder(5,0,5,0));
        // JLabel para o nome
        JLabel lb = new JLabel("Nome da Delegação: ");
        txtNomeDelegacao = new JTextField(20);
        // adicionar label ao panel
        pLinha.add(lb);
        pLinha.add(txtNomeDelegacao);
        // adicionar painel ao gridlayout
        pGrid.add(pLinha);
        // *********************************************************************
        
        TrataEvento t = new TrataEvento();
        
        // SOUTH DO BORDERLAYOUT************************************************
        // criar painel para botões
        JPanel pBtn = new JPanel();
        pBtn.setBorder(new EmptyBorder(5,0,5,0));
        // botão OK
        btOK = new JButton("OK");
        btOK.addActionListener(t);
        // fazer actionlistener para OK
        pBtn.add(btOK);
        // botao CANCELAR
        btCancel = new JButton("Cancelar");
        btCancel.addActionListener(t);
        pBtn.add(btCancel);
        // colocar botao OK como default
        getRootPane().setDefaultButton(btOK);
        
        // adicionar pGrid ao CENTER do BorderLayout
        add(pGrid, BorderLayout.CENTER);
        // adicionar pBtn ao SOUTH do BorderLayout
        add(pBtn, BorderLayout.SOUTH);
        
        
        // settings para dimensoes da janela e localização
        pack();
        setMinimumSize(new Dimension(getWidth(),getHeight()));
        setLocation(pai.getX()+50, pai.getY()+50);  
    }
    
    // alteração de delegações
    public void setDelegacao(Delegacao d){
        txtNomeDelegacao.setText(d.getPais());
    }
    
    
    // tratamento de eventos
    private class TrataEvento implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // botão CANCEL
            if(e.getSource()==btCancel){
                delegacao=null;
                dispose();
            }
            // botão OK
            else{
                if(txtNomeDelegacao.getText().isEmpty())
                    JOptionPane.showMessageDialog(DialogDelegacao.this,"Introduza um nome para a Delegação","Criar Delegação",JOptionPane.WARNING_MESSAGE);
                else{
                    String nome = txtNomeDelegacao.getText();
                    delegacao = new Delegacao(nome,new Medalhas());
                    dispose();
                }      
            }
        }
    }
    
    public Delegacao getDelegacao(){
        return delegacao;
    }
    
}
