
import Library.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.*;

/* Janela Principal do programa
 * 
 */
public class Janela extends JFrame {

    //  *** VARIÁVEIS DE CLASSE ***

    
    //  *** VARIÁVEIS DE INSTÂNCIA ***


    //  *** CONSTRUTORES ***

    // construtor da janela
    public Janela(String titulo){
        // titulo da janela = titulo da superclasse
        super(titulo);
        
        // declarar menubar, JMenu e menuitems
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;
        
        // criar menubar
        menuBar = new JMenuBar();
        
        
        
        // menu PRINCIPAL*******************************************************************************************************************************************************************************************************
        menu = new JMenu("Menu Principal");
        menu.setMnemonic('M');
        menuBar.add(menu);
        
        // menuItens CARREGAR DELEGAÇÃO
        menuItem = new JMenuItem("Carregar Delegação",'1');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl 1"));
        // actionListener para carregar delegações a partir de txt
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                Main.carregarDelegacoesTxt();
            }
        });
        menu.add(menuItem);
        
        // menuItens CARREGAR MODALIDADE
        menuItem = new JMenuItem("Carregar Modalidades",'2');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl 2"));
        // actionListener para carregar modalidades a partir de txt
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                Main.carregarModaliadadesTxt();
            }
        });
        menu.add(menuItem);
        
//        // menuItens CARREGAR RESULTADOS
//        menuItem = new JMenuItem("Carregar Resultados",'R');
//        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl R"));
//        /*
//         * fazer actionlistener para Carregar Resultados a partir de txt
//         */
//        menu.add(menuItem);
        
        // separador
        menu.addSeparator();
        
         // menuItens GRAVAR
        menuItem = new JMenuItem("Gravar",'3');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl 3"));
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                gravar();
            }
        });
        menu.add(menuItem);
        
         // menuItens ELIMINAR TUDO
        menuItem = new JMenuItem("Eliminar",'4');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl 4"));
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                eliminar();
            }
        });
        menu.add(menuItem);
        
        // separador
        menu.addSeparator();
        
        // menuItens SAIR 
        menuItem = new JMenuItem("Sair",'5');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl 5"));
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                fechar();
            }
        });
        menu.add(menuItem);
        //**********************************************************************************************************************************************************************************************************************************************
 
 
        
        // menu DELEGAÇÃO*******************************************************************************************************************************************************************************************************************************
        menu = new JMenu("Delegação");
        menu.setMnemonic('D');
        menuBar.add(menu);
        
        // menuItens CRIAR DELEGAÇÃO do menu DELEGAÇÃO
        menuItem = new JMenuItem("Criar Delegação",'C');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl C"));
        // actionListener para criar DELEGAÇÃO
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent a){
                DialogDelegacao dialog = new DialogDelegacao(Janela.this,"Criar Delegação");
                dialog.setVisible(true);
                Delegacao delegacao = dialog.getDelegacao();
                if(delegacao != null)
                    Main.getDelegacao().add(delegacao);
            }
            });
        menu.add(menuItem);
        
        // menuItens EDITAR DELEGAÇÃO do menu DELEGAÇÃO
        menuItem = new JMenuItem("Editar Delegação",'E');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));
        // actionListener para editar DELEGAÇÃO
        menuItem.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               if(Main.getDelegacao().isEmpty())
                   JOptionPane.showMessageDialog(Janela.this,"Não existem Delegações!","Editar Delegação",JOptionPane.WARNING_MESSAGE);
               else{
                   Object obj = JOptionPane.showInputDialog(Janela.this,"Escolha a Delegação:","Editar Delegação",JOptionPane.PLAIN_MESSAGE,null,Main.nomesDelegacoes(),Main.nomesDelegacoes()[0]);
                   if(obj!=null){
                       Delegacao del = Main.delegPorNome((String)obj);
                       DialogDelegacao dialog = new DialogDelegacao(Janela.this,"Alteração de Delegação");
                       dialog.setDelegacao(del);
                       dialog.setVisible(true);
                       Delegacao del2 = dialog.getDelegacao();
                       if(del2!=null)
                           Main.getDelegacao().set(Main.getDelegacao().indexOf(del),del2);   
                   }
               }            
           }
        });
        menu.add(menuItem);
        
        // menuItens ELIMINAR DELEGAÇÃO do menu DELEGAÇÃO
        menuItem = new JMenuItem("Eliminar Delegação",'X');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));
        // actionListener para eliminar DELEGAÇÃO
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(Main.getDelegacao().isEmpty())
                    JOptionPane.showMessageDialog(Janela.this,"Não existem Delegações!","Eliminar Delegação",JOptionPane.WARNING_MESSAGE);
                else{
                    Object obj = JOptionPane.showInputDialog(Janela.this,"Escolha a Delegação:","Eliminar Delegação",JOptionPane.PLAIN_MESSAGE,null,Main.nomesDelegacoes(),Main.nomesDelegacoes()[0]);
                    if(obj!=null){
                        Object[] opSN = {"Sim","Não"};
                        if(JOptionPane.showOptionDialog(Janela.this,"Confirma eliminar?","Eliminar Delegação",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,opSN,opSN[1])==JOptionPane.YES_OPTION)
                            Main.getDelegacao().remove(Main.delegPorNome((String)obj));
                        }
                }
            }
        });
        menu.add(menuItem);
        
        //separador
        menu.addSeparator();
        
        // submenu LISTAR DELEGAÇÃO do menu DELEGAÇÃO
        JMenu submenu = new JMenu("Listar Delegações");
        submenu.setMnemonic('L');
        
        // menuItem 1 do subMenu
        menuItem = new JMenuItem("Ecrã",'E');
        // actionListener para Listar DELEGAÇÕES
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                DialogListarDelegacoes dialog = new DialogListarDelegacoes(Janela.this,"Listar Delegações");
            }
        });
        submenu.add(menuItem);
        
        //meuItem 2 do subMenu
        menuItem = new JMenuItem("Ficheiro",'F');
        // actionListener para exportar Delegações para ficheiro de txt
        menuItem.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               Main.listarDelegFicheiro();
           }
        });
        /*
         * fazer actionlistener p listar em ficheiro
         */
        submenu.add(menuItem);
        menu.add(submenu);
        //**********************************************************************************************************************************************************************************************************************
        
        
        
        // menu MODALIDADE*************************************************************************************************************************************************************************************
        menu = new JMenu("Modalidade");
        menu.setMnemonic('M');
        menuBar.add(menu);
        
        // menuItens CRIAR MODALIDADE do menu MODALIDADE
        menuItem = new JMenuItem("Criar Modalidade",'M');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl M"));
        // actionListener para criar DELEGAÇÃO
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent a){
                DialogModalidade dialog = new DialogModalidade(Janela.this,"Criar Modalidade");
                dialog.setVisible(true);
                Modalidade modalidade = dialog.getModalidade();
                if(modalidade != null)
                    Main.getModalidade().add(modalidade);
            }
            });
        menu.add(menuItem);
        
        // menuItens EDITAR MODALIDADE do menu MODALIDADE
        menuItem = new JMenuItem("Editar Modalidade",'N');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl N"));
        // actionListener para editar DELEGAÇÃO
        menuItem.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               if(Main.getModalidade().isEmpty())
                   JOptionPane.showMessageDialog(Janela.this,"Não existem Modalidades!","Editar Modalidade",JOptionPane.WARNING_MESSAGE);
               else{
                   Object obj = JOptionPane.showInputDialog(Janela.this,"Escolha a Modalidade:","Editar Modalidade",JOptionPane.PLAIN_MESSAGE,null,Main.nomesModalidades(),Main.nomesModalidades()[0]);
                   if(obj!=null){
                       Modalidade m = Main.modalidPorNome((String)obj);
                       DialogModalidade dialog = new DialogModalidade(Janela.this,"Alteração de Modalidade");
                       dialog.setModalidade(m);
                       dialog.setVisible(true);
                       Modalidade m2 = dialog.getModalidade();
                       if(m2!=null)
                           Main.getModalidade().set(Main.getModalidade().indexOf(m),m2);   
                   }
               }            
           }
        });
        menu.add(menuItem);
        
        // menuItens ELIMINAR MODALIDADE do menu MODALIDADE
        menuItem = new JMenuItem("Eliminar Modalidade",'Z');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Z"));
        // actionListener para eliminar DELEGAÇÃO
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(Main.getModalidade().isEmpty())
                    JOptionPane.showMessageDialog(Janela.this,"Não existem Modalidades!","Eliminar Modalidade",JOptionPane.WARNING_MESSAGE);
                else{
                    Object obj = JOptionPane.showInputDialog(Janela.this,"Escolha a Modalidade:","Eliminar Modalidade",JOptionPane.PLAIN_MESSAGE,null,Main.nomesModalidades(),Main.nomesModalidades()[0]);
                    if(obj!=null){
                        Object[] opSN = {"Sim","Não"};
                        if(JOptionPane.showOptionDialog(Janela.this,"Confirma eliminar?","Eliminar Modalidade",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,opSN,opSN[1])==JOptionPane.YES_OPTION)
                            Main.getModalidade().remove(Main.modalidPorNome((String)obj));
                        }
                }
            }
        });
        menu.add(menuItem);
        
        //separador
        menu.addSeparator();
        
        // submenu LISTAR MODALIDADE do menu MODALIDADE
        JMenu submenu3= new JMenu("Listar Modalidades");
        submenu3.setMnemonic('L');
        
        // menuItem 1 do subMenu
        menuItem = new JMenuItem("Ecrã",'E');
        // actionListener para Listar Modalidades
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                DialogListarModalidade dialog = new DialogListarModalidade(Janela.this,"Listar Modalidades");
            }
        });
        submenu3.add(menuItem);
        
        //meuItem 2 do subMenu
        menuItem = new JMenuItem("Ficheiro",'F');
        // actionListener para exportar Delegações para ficheiro de txt
        menuItem.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               Main.listarModalidFicheiro();
           }
        });
        
        submenu3.add(menuItem);
        menu.add(submenu3);
        //**********************************************************************************************************************************************************************************************************************************************
        
        
        
        // menu RESULTADOS*******************************************************************************************************************************************************************************************************************************************************
        menu = new JMenu("Resultados");
        menu.setMnemonic('R');
        menuBar.add(menu);
        
        // menuItens CRIAR RESULTADO do menu RESULTADOS
        menuItem = new JMenuItem("Criar Resultado",'R');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl R"));
        // actionListener para CRIAR RESULTADO do menu RESULTADOS
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){     
                if(Main.getDelegacao().isEmpty())
                    JOptionPane.showMessageDialog(Janela.this,"Não existem Delegações!","Criar Resultado",JOptionPane.WARNING_MESSAGE);
                else if(Main.getModalidade().isEmpty())
                    JOptionPane.showMessageDialog(Janela.this,"Não existem Modalidades!","Criar Resultado",JOptionPane.WARNING_MESSAGE);
                else{
                    Object objD = JOptionPane.showInputDialog(Janela.this,"Escolha uma Delegação:","Criar Resultado",JOptionPane.PLAIN_MESSAGE,null,Main.nomesDelegacoes(),Main.nomesDelegacoes()[0]);
                    Delegacao d = Main.delegPorNome((String)objD);
                    Object objM = JOptionPane.showInputDialog(Janela.this,"Escolha uma Modalidade:","Criar Resultado",JOptionPane.PLAIN_MESSAGE,null,Main.nomesModalidades(),Main.nomesModalidades()[0]);
                    Modalidade m = Main.modalidPorNome((String)objM);   
                    try{
                    int res = Integer.parseInt(JOptionPane.showInputDialog(Janela.this,"Indique o resultado final:","Criar Resultado",JOptionPane.QUESTION_MESSAGE));          
                    Resultado resultado = new Resultado(d,m,res);
                    Main.getResultado().add(resultado);
                    // atribui medalhas assim que é criado um resultado
                    Delegacao.limparMedalhas(Main.getDelegacao());
                    Resultado.atribuirMedalha(Main.getResultado());
                    }
                    catch(NumberFormatException exc){
                        JOptionPane.showMessageDialog(Janela.this,"Resultado tem que ser um valor númerico!","Criar Resultado",JOptionPane.WARNING_MESSAGE);
                    }
                    catch(ResultadoIncorrecto exc){
                        JOptionPane.showMessageDialog(Janela.this,exc.getMessage(),"Criar Resultado",JOptionPane.WARNING_MESSAGE);
                    }
                }   
            }
        }); 
        menu.add(menuItem);
        
        // menuItens EDITAR RESULTADO do menu RESULTADOS
        menuItem = new JMenuItem("Editar Resultado",'T');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl T"));
        // actionListener para editar RESULTADO
        menuItem.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               if(Main.getResultado().isEmpty())
                   JOptionPane.showMessageDialog(Janela.this,"Não existem Resultados!","Editar Resultado",JOptionPane.WARNING_MESSAGE);
               else{
                   Object obj = JOptionPane.showInputDialog(Janela.this,"Escolha o Resultado:","Editar Resultado",JOptionPane.PLAIN_MESSAGE,null,Main.nomesResultados(),Main.nomesResultados()[0]);
                   if(obj!=null){
                       Resultado r = Main.resultPorNome((String)obj);
                       try{
                        int res = Integer.parseInt(JOptionPane.showInputDialog(Janela.this,"Indique novo resultado final:","Editar Resultado",JOptionPane.QUESTION_MESSAGE));          
                        // atribui medalhas assim que é editado um resultado
                        Delegacao.limparMedalhas(Main.getDelegacao());
                        r.setResult(res);
                        Resultado.atribuirMedalha(Main.getResultado());
                        }
                        catch(NumberFormatException exc){
                            JOptionPane.showMessageDialog(Janela.this,"Resultado tem que ser um valor númerico!","Criar Resultado",JOptionPane.WARNING_MESSAGE);
                        }
                        catch(ResultadoIncorrecto exc){
                            JOptionPane.showMessageDialog(Janela.this,exc.getMessage(),"Criar Resultado",JOptionPane.WARNING_MESSAGE);
                        }
                   }
               }            
           }
        });
        menu.add(menuItem);
        
        // menuItens ELIMINAR RESULTADO do menu RESULTADOS
        menuItem = new JMenuItem("Eliminar Resultado",'Y');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Y"));
        // actionListener para eliminar RESULTADO
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(Main.getResultado().isEmpty())
                    JOptionPane.showMessageDialog(Janela.this,"Não existem Resultados!","Eliminar Resultado",JOptionPane.WARNING_MESSAGE);
                else{
                    Object obj = JOptionPane.showInputDialog(Janela.this,"Escolha o Resultado:","Eliminar Resultado",JOptionPane.PLAIN_MESSAGE,null,Main.nomesResultados(),Main.nomesResultados()[0]);
                    if(obj!=null){
                        Object[] opSN = {"Sim","Não"};
                        if(JOptionPane.showOptionDialog(Janela.this,"Confirma eliminar?","Eliminar Modalidade",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,opSN,opSN[1])==JOptionPane.YES_OPTION){
                            Main.getResultado().remove(Main.resultPorNome((String)obj));
                            // eliminar medalha respectiva (elimina todas e volta a atribuir com base no novo arraylist
                            Delegacao.limparMedalhas(Main.getDelegacao());
                            Resultado.atribuirMedalha(Main.getResultado());
                            }
                        }
                }
            }
        });
        menu.add(menuItem);
        
        //separador
        menu.addSeparator();
        
        // submenu LISTAR RESULTADOS do menu RESULTADOS
        JMenu submenu2 = new JMenu("Listar Resultados");
        submenu.setMnemonic('R');
        
        // menuItem 1 do subMenu
        menuItem = new JMenuItem("Ecrã",'E');
        // actionListener para listar resultados no ecrã
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                DialogListarResultados dialog = new DialogListarResultados(Janela.this,"Listar Resultados");
            }
        });
        submenu2.add(menuItem);
        
        //meuItem 2 do subMenu
        menuItem = new JMenuItem("Ficheiro",'F');
        menuItem.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               Main.listarResultFicheiro();
           }
        });
        
        submenu2.add(menuItem);
        menu.add(submenu2);
        //**********************************************************************************************************************************************************************************************************************************************
        
        
        
        // menu MEDALHAS*******************************************************************************************************************************************************************************************************
        menu = new JMenu("Medalhas");
        menu.setMnemonic('M');
        menuBar.add(menu);
        
        // menuItens LISTAR MEDALHAS DE UMA DELEGAÇÃO do menu MEDALHAS
        menuItem = new JMenuItem("Ver Medalhas de uma Delegação",'V');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl V"));
        menuItem.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               if(Main.getDelegacao().isEmpty())
                   JOptionPane.showMessageDialog(Janela.this,"Não existem Delegações!","Ver Medalhas de uma Delegação",JOptionPane.WARNING_MESSAGE);
               else{
                   DialogMedalhasDelegacao dialog = new DialogMedalhasDelegacao(Janela.this,"Medalhas de uma Delegação");
               }
           }
        });
//        // ALTERNATIVA: JOPTION PANE
//        // actionListener para ver medalhas de uma delegação
//        menuItem.addActionListener(new ActionListener(){
//           @Override
//           public void actionPerformed(ActionEvent e){
//               if(Main.getDelegacao().isEmpty())
//                   JOptionPane.showMessageDialog(Janela.this,"Não existem Delegações!","Ver Medalhas de uma Delegação",JOptionPane.WARNING_MESSAGE);
//               else{
//                   Object obj = JOptionPane.showInputDialog(Janela.this,"Escolha a Delegação:","Ver Medalhas de uma Delegação",JOptionPane.PLAIN_MESSAGE,null,Main.nomesDelegacoes(),Main.nomesDelegacoes()[0]);
//                   if(obj!=null){
//                       Delegacao del = Main.delegPorNome((String)obj);
//                       JOptionPane.showMessageDialog(Janela.this, del.toString(),"Ver Medalhas de uma Delegação",JOptionPane.PLAIN_MESSAGE);
//                       
//                       
//                       
//                   }
//               }            
//           }
//        });
        
        menu.add(menuItem);
        
        // classificação por total de MEDALHAS
        menuItem = new JMenuItem("Classificação por Total Medalhas",'H');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl H"));
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                // ordena arraylist por total de medalhas obtidas
                Medalhas.classifPorDeleg(Main.getDelegacao());
                DialogClassificacao dialog = new DialogClassificacao(Janela.this,"Classificação por Total de Medalhas");
            }
        });
        menu.add(menuItem);
        
        // menuItens QUADRO DE HONRA QUALIFICADO do menu MEDALHAS
        menuItem = new JMenuItem("Classificação por Total Qualificado",'Q');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Q"));
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                // ordena arraylist por total de medalhas obtidas
                Medalhas.classifPorDelegQual(Main.getDelegacao());
                DialogClassificacaoQualif dialog = new DialogClassificacaoQualif(Janela.this,"Classificação por Total Qualificado");
            }
        });
        menu.add(menuItem);
        
        menu.add(menuItem);
        
        // menuItens PAÍSES SEM MEDALHAS do menu MEDALHAS
        menuItem = new JMenuItem("Delegações sem Medalhas",'O');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl O"));
        // actionListener para Listar DELEGAÇÕES
        menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                DialogListarDelegacoesSemMedalhas dialog = new DialogListarDelegacoesSemMedalhas(Janela.this,"Delegações sem Medalhas");
            }
        });
        submenu.add(menuItem);
        menu.add(menuItem);
        
        // separador
        menu.addSeparator();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
//         // menuItens PAÍSES SEM MEDALHAS do menu MEDALHAS
//        menuItem = new JMenuItem("Classificação por Modalidade");
//        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
//        // actionListener para Listar DELEGAÇÕES
//        menuItem.addActionListener(new ActionListener(){
//            @Override
//            public void actionPerformed(ActionEvent e){
//                if(Main.getModalidade().isEmpty())
//                   JOptionPane.showMessageDialog(Janela.this,"Não existem Modalidades!","Classificação por Modalidade",JOptionPane.WARNING_MESSAGE);
//                else{
//                    ArrayList<Resultado> copia = Main.getResultado();
//                    Object obj = JOptionPane.showInputDialog(Janela.this,"Escolha a Modalidade:","Classificação por Modalidade",JOptionPane.PLAIN_MESSAGE,null,Main.nomesModalidades(),Main.nomesModalidades()[0]);
//                    Modalidade m = Main.modalidPorNome((String)obj);
//                    
//                    
//                    int i=0;
//                    
//                    
//                    for(Resultado r : copia){
//                        if(!r.getModalidade().getModalidade().equalsIgnoreCase(m.getModalidade()))
//                            remove(i);
//                        i++;
//                }
//                    
//                Delegacao.limparMedalhas(Main.getDelegacao());
//                Resultado.atribuirMedalha(copia);
//                }    
//                Medalhas.classifPorDelegQual(Main.getDelegacao());
//                DialogClassificacao dialog = new DialogClassificacao(Janela.this,"Classificação por Modalidade");
//                Delegacao.limparMedalhas(Main.getDelegacao());
//                Resultado.atribuirMedalha(Main.getResultado());
//            }
//        });
//        submenu.add(menuItem);
//        menu.add(menuItem);
//        // separador
//        menu.addSeparator();
        
        
        
        
        // menuItens GRÁFICO TOP5 do menu MEDALHAS
        // menuItens GRÁFICO TOP5 do menu MEDALHAS
        menuItem = new JMenuItem("Ver Gráfico Top5",'T');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl T"));
        // actionListener para TOP5
         menuItem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                Medalhas.classifPorMedalha(Main.getDelegacao());
                DialogTop5 dialog = new DialogTop5(Janela.this,"Grafico Top 5");
            }
         });
        menu.add(menuItem);
        //**********************************************************************************************************************************************************************************************************************************************
        
        
        
        // menu ACERCA*******************************************************************************************************************************************************************************************************
        menu = new JMenu("Acerca");
        menu.setMnemonic('A');
        menuBar.add(menu);
        
        // menuItens Autores
        menuItem = new JMenuItem("Autores",'a');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl A"));
        menuItem.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
                   JOptionPane.showMessageDialog(Janela.this,"Software desenvolvido por:\nJoão Silva (nº 1040314)\nJoão Carreira (nº 1111168)\n\nParadigmas da Programação\nLicenciatura em Eng. Informática\nISEP - Porto, Maio 2012","Autores",JOptionPane.PLAIN_MESSAGE);
           }
        });
        menu.add(menuItem);
        //**************************************************************************************************************************************************************************************
        
        
        
        // adicionar menuBar
        setJMenuBar(menuBar);
        
        // settings standard para janela principal
        add(new JLabel(new ImageIcon("logo.jpg")));
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                fechar();
            }
        });
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();
        setSize(850,500);
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    

    //  *** MÉTODOS DE CLASSE ***

    // método para confirmar fechar da aplicação
    private void fechar(){
        Object opSimNao[] = {"Sim","Não"};
        if(JOptionPane.showOptionDialog(Janela.this,"Deseja sair da aplicação?","Gestão de Medalhas Olímpicas",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,opSimNao,opSimNao[1])==0)
            Main.gravarEstado();
            dispose();       
    }
    
    // método para gravar todos os dados nos ficheiros bin
    private void gravar(){
        Object opSimNao[] = {"Sim","Não"};
        if(JOptionPane.showOptionDialog(Janela.this,"Pretende gravar todos os dados? \nNOTA: Dados anteriores serão re-escritos!","Gravar",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,opSimNao,opSimNao[1])==0)
            Main.gravarEstado();       
    }
    
    // método para eliminar toda a informação
    private void eliminar(){
        Object opSimNao[] = {"Sim","Não"};
        if(JOptionPane.showOptionDialog(Janela.this,"Pretende eliminar toda a informação? \nNOTA: Dados anteriores não serão recuperados!","Eliminar",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,opSimNao,opSimNao[1])==0){
            Main.getDelegacao().clear();
            Main.getModalidade().clear();
            Main.getResultado().clear();
            Main.gravarEstado();
        }
    }
}
